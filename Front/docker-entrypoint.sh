#!/bin/sh

if [[ ! -z "${NS_PORTAL_FRONT_CORE_URL}" ]]; then
  sed -i "s/coreUrl:.*'.*'/coreUrl: '${NS_PORTAL_FRONT_CORE_URL}'/g" /usr/share/nginx/html/build/prod.js
fi

nginx -g "daemon off;"
